export const apiUrls = {
    register : 'api/user/register',
    login : 'api/users/authenticate',
    forgotPassword : 'api/user/fogotPassword',
    resetPassword : 'api/user/resetPassword'
}

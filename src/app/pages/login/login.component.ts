import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { emailValidator, matchingPasswords } from '../../theme/utils/app-validators';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import {ToastrService} from 'ngx-toastr';
import * as $ from 'jquery';
import {ApiService} from '../../services/api.service';
import {apiUrls} from '../../../environments/apiUrls';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements AfterViewInit {
  loginForm: FormGroup;
  forgotForm: FormGroup;
  resetForm: FormGroup;
  loading: boolean;
  returnUrl: string;

  public settings: Settings;
  constructor(public appSettings: AppSettings, public fb: FormBuilder,
              public router: Router, private toastr: ToastrService,
              private apiService: ApiService, private dataService: DataService, private route: ActivatedRoute) {
    this.settings = this.appSettings.settings;
    this.loginForm = this.fb.group({
      email: [null, Validators.compose([Validators.required, emailValidator])],
      password: ['', [Validators.required]]
    });

    this.forgotForm = this.fb.group({
      email: [null, Validators.compose([Validators.required, emailValidator])]
    });

    this.resetForm = this.fb.group({
      otp: ['', [Validators.required]],
      newPassword: ['', [Validators.required, Validators.pattern('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$')]],
      confirmNewPassword: ['', [Validators.required]],
    }, {
      validator: matchingPasswords('newPassword', 'confirmNewPassword')
    });
    // get return url from route parameters or default to '/'
    // this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/dashboard';
    localStorage.clear();
  }

  ngAfterViewInit() {
    $('#to-recover').on('click', () => {
      this.loginForm.reset();
      $('#loginForm').slideUp();
      $('#recoverForm').fadeIn();
    });
    this.settings.loadingSpinner = false;
  }

  get f() {
    return this.loginForm.controls;
  }
  get f1() {
    return this.forgotForm.controls;
  }
  get f2() {
    return this.resetForm.controls;
  }

  restBack() {
    this.forgotForm.reset();
    this.resetForm.reset();
    $('#resetPasswordForm').slideUp();
    $('#loginForm').fadeIn();
  }

  login() {
    if (this.loginForm.valid) {
      const postData = {
        email : this.f.email.value,
        password : this.f.password.value
      }
      this.apiService.post(apiUrls.login, postData).subscribe((data: any) => {
        if (data.statusCode === 200) {
          this.toastr.success(data.message);
          this.dataService.saveCurrentUser(data.userData);
          this.router.navigate(['/dashboard']);
        }
        else if (data.statusCode === 404) {
          this.toastr.warning(data.message);
        }
        else if (data.statusCode === 401) {
          this.toastr.warning(data.message);
        }
      }, error => {
        this.toastr.error(error);
      });
    }
  }

  register() {
    this.router.navigate(['/register']);
  }

  loginPage() {
    this.forgotForm.reset();
    this.resetForm.reset();
    $('#loginForm').fadeIn();
    $('#recoverForm').slideUp();
  }

  forgotPassword() {
    if (this.forgotForm.valid) {
      this.settings.loadingSpinner = true;
      this.apiService.post(apiUrls.forgotPassword, {email : this.f1.email.value}).subscribe((data: any) => {
         if (data.status === 200) {
           this.settings.loadingSpinner = false;
           localStorage.setItem('userId', data.userId)
           this.toastr.success(data.message);
           $('#recoverForm').slideUp();
           $('#resetPasswordForm').fadeIn();
         } else {
           this.settings.loadingSpinner = false;
           this.toastr.warning(data.message);
         }
       },
       error => {
         this.settings.loadingSpinner = false;
         this.toastr.error(error);
       });
    }

  }

  resetPassword() {
    if (this.resetForm.valid) {
      const resetData = {
        userId: localStorage.getItem('userId'),
        newPassword: this.resetForm.value.newPassword,
        otp: this.resetForm.value.otp
      }
      this.apiService.post(apiUrls.resetPassword, resetData).subscribe((data: any) => {
        if (data.status === 200) {
          this.toastr.success(data.message);
          this.resetForm.reset();
          $('#resetPasswordForm').slideUp();
          $('#loginForm').fadeIn();
        } else {
          this.toastr.warning(data.message);
        }
      });
    }
  }
}

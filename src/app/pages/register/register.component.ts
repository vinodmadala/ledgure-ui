import {AfterViewInit, Component} from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { emailValidator, matchingPasswords } from '../../theme/utils/app-validators';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import {ToastrService} from "ngx-toastr";
import {ApiService} from "../../services/api.service";
import {apiUrls} from "../../../environments/apiUrls";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements AfterViewInit {
  public registerForm: FormGroup;
  public settings: Settings;
  constructor(public appSettings: AppSettings, public fb: FormBuilder, public router: Router,
              private toastr: ToastrService, private apiService: ApiService) {
    this.settings = this.appSettings.settings; 
    this.registerForm = this.fb.group({
      'firstName': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'lastName': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'email': [null, Validators.compose([Validators.required, emailValidator])],
      'mobileNumber': ['', Validators.required],
      'password': ['', Validators.required],
      'confirmPassword': ['', Validators.required]
    }, {validator: matchingPasswords('password', 'confirmPassword')});
  }

  get f() {
    return this.registerForm.controls;
  }

  register() {
    if (this.registerForm.valid) {
      const registerData = {
        firstName : this.f.firstName.value,
        lastName : this.f.lastName.value,
        email : this.f.email.value,
        mobileNumber : this.f.mobileNumber.value,
        password : this.f.password.value,
      };

      console.log(JSON.stringify(registerData));

      this.apiService.post(apiUrls.register, registerData).subscribe((data: any) => {
            if (data.statusCode == 200) {
              this.toastr.success(data.message);
              this.router.navigate(['/login']);
            } else if (data.statusCode == 409) {
              this.toastr.warning(data.message);
            }
          }, (error: any) => {
              this.toastr.error(error);
      });
    }
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;
  }
}

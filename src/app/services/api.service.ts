import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { DataService } from './data.service';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { ConfigService } from './apiConfig.service';

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  bearerToken: string = '';
  baseUrl: string= '';


  constructor(private http: HttpClient, private dataService: DataService,private configService: ConfigService) {
    // setTimeout(() => {
    //   this.setBaseUrl();
    // },0);searchValue

    this.setBaseUrl();
  }
  setBaseUrl() {
    return new Promise(resolve => {
      this.baseUrl = this.configService.getConfiguration().BaseUrl;
      resolve(this.baseUrl);
    });
  }

  setBasicToken() {
    return new Promise(resolve => {
      this.bearerToken = this.configService.getConfiguration().token;
      resolve(this.bearerToken);
    });
  }

  get(url) {
    return this.http.get(this.baseUrl + url, this.appendHeaders()).pipe(
      map((respo) => {
        return respo;
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
          //this.dataService.logOutAdmin();
        }
        return throwError(error);
      }));
  }

  post(url, data) {
    return this.http.post(this.baseUrl + url, data, this.appendHeaders()).pipe(
      map((respo) => {
        return respo;
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
        //  this.dataService.logOutAdmin();
        }
        return throwError(error);
      }));
  }

  put(url, data) {
    return this.http.put(this.baseUrl + url, data, this.appendHeaders()).pipe(
      map((respo) => {
        return respo;
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
        //  this.dataService.logOutAdmin();
        }
        return throwError(error);
      }));
  }

  delete(url) {
    return this.http.delete(this.baseUrl + url, this.appendHeaders()).pipe(
      map((respo) => {
        return respo;
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
        //  this.dataService.logOutAdmin();
        }
        return throwError(error);
      }));
  }

  appendHeaders() {
      //const accessToken = this.dataService.getAccessToken();
      const accessToken = null;
      if (accessToken) {
        let headers = new HttpHeaders({ 'Content-Type': ('application/x-www-form-urlencoded') });
        headers = headers.append('Authorization', ('Bearer ' + accessToken));
        return { headers };
      } else {
       // this.dataService.logOutAdmin();
      }
  }
}

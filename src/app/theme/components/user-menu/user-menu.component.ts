import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {DataService} from '../../../services/data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UserMenuComponent implements OnInit {
  public userImage = '../assets/img/users/user.jpg';
  constructor(private dataService: DataService) { }

  ngOnInit() {
  }
  logout() {
    this.dataService.logout();
  }

}
